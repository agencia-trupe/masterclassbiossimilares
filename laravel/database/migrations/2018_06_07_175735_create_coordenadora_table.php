<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCoordenadoraTable extends Migration
{
    public function up()
    {
        Schema::create('coordenadora', function (Blueprint $table) {
            $table->increments('id');
            $table->string('foto');
            $table->string('nome');
            $table->text('descricao');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('coordenadora');
    }
}
