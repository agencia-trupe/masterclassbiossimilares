<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVideosRelacionadosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('videos_relacionados', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('video_id')->unsigned()->nullable();
            $table->integer('relacionado_id')->unsigned()->nullable();
            $table->integer('ordem')->default(0);
            $table->foreign('video_id')->references('id')->on('videos')->onDelete('cascade');
            $table->foreign('relacionado_id')->references('id')->on('videos')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('videos_relacionados');
    }
}
