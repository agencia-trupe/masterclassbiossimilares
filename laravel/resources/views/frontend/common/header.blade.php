    <header>
        <div class="center">
            <a href="{{ route('home') }}" class="logo">SANDOZ</a>
            @if(Auth::guard('cadastro')->check())
                <a href="{{ route('logoutCadastro') }}" class="area-restrita">
                    <div>
                        <span class="nome">
                            Olá {{ strtoupper(explode(' ', Auth::guard('cadastro')->user()->nome)[0]) }}
                        </span>
                        <span class="logout"></span>
                    </div>
                </a>
            @else
                <a href="{{ route('loginCadastro') }}" class="area-restrita">
                    <div>ÁREA RESTRITA</div>
                </a>
            @endif
        </div>
        <nav>
            <div class="center">
                @include('frontend.common.nav')
            </div>
        </nav>
    </header>
