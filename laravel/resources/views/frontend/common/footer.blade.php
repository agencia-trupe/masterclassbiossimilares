    <footer>
        <div class="center">
            <div class="col">
                <img src="{{ asset('assets/img/layout/marca-sobrafo.png') }}" alt="">
            </div>
            <div class="col">
                <span>Apoio:</span>
                <img src="{{ asset('assets/img/layout/marca-sandoz.png') }}" alt="">
            </div>
            <div class="col">
                <span>Realização:</span>
                <img src="{{ asset('assets/img/layout/marca-segmentofarma.png') }}" alt="">
            </div>
        </div>
    </footer>
