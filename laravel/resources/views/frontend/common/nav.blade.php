<a href="{{ route('home') }}" @if(Tools::isActive('home')) class="active" @endif>Home</a>
<a href="{{ route('sobre') }}" @if(Tools::isActive('sobre')) class="active" @endif>Sobre o Programa</a>
<a href="{{ route('aulas') }}" @if(Tools::isActive('aulas')) class="active" @endif>Aulas</a>
<a href="{{ route('palestrantes') }}" @if(Tools::isActive('palestrantes')) class="active" @endif>Palestrantes e Coordenadora</a>
<a href="{{ route('certificado') }}" @if(Tools::isActive('certificado')) class="active" @endif>Certificado</a>
