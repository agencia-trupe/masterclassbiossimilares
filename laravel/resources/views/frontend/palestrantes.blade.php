@extends('frontend.common.template')

@section('content')

    <div class="palestrantes">
        <div class="center">
            @foreach($palestrantes as $palestrante)
            <div class="wrapper">
                <img src="{{ asset('assets/img/palestrantes/'.$palestrante->foto) }}" alt="">
                <div class="texto">
                    <h2>{{ $palestrante->nome }}</h2>
                    {!! $palestrante->texto !!}
                </div>
            </div>
            @endforeach
        </div>
    </div>

@endsection
