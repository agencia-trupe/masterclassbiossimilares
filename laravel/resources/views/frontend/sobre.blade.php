@extends('frontend.common.template')

@section('content')

    <div class="sobre">
        <div class="center">
            <div class="texto">
                <h2>SOBRE O PROGRAMA</h2>
                {!! $sobre->texto !!}
                <div class="video-wrapper">
                    <iframe src="https://player.vimeo.com/video/273775299" width="640" height="360" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
                </div>
            </div>
        </div>
    </div>

    <div class="lista-aulas-grande">
        <div class="center">
            @foreach($aulas as $aula)
                <a href="{{ route('aulas', $aula->slug) }}">
                    <div class="imagem">
                        <img src="{{ asset('assets/img/videos/'.$aula->foto) }}" alt="">
                    </div>
                    <div class="texto">
                        <p>{{ $aula->autor }}</p>
                        <h3>{{ $aula->titulo }}</h3>
                        <p class="descricao">{!! strip_tags($aula->descricao, '<br>') !!}</p>
                    </div>
                </a>
            @endforeach
        </div>
    </div>

@endsection
