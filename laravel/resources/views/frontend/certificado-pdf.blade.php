<html>
<head>
<link href='http://fonts.googleapis.com/css?family=Kaushan+Script' rel='stylesheet' type='text/css'>
<style>
    @page { margin: 0in; }
    body {
        font-family: Kaushan Script, sans-serif;
        padding: 0;
        width:100%;
        height:100%;
    }
    .bg {
        position: absolute;
        z-index: -1;
        top: 0;
        left: 0;
        width: 1122px;
        height: 793px;
    }
    .box-nome {
        display: table;
        position: absolute;
        top: 50%;
        left: 0;
        width: 100%;
        height: 200px;
        margin-top: -130px;
    }
    .nome {
        display: table-cell;
        vertical-align: middle;
        color: #3872B3;
        font-size: 48pt;
        line-height: .5;
        padding: 0 74px;
    }
</style>
</head>
<body>
    <img src="assets/img/layout/certificado_masterclassBiossimilares-template.png" class="bg" alt="">
    <div class="box-nome">
        <p class="nome">{{ $nome }}</p>
    </div>
</body>
</html>
