@extends('frontend.common.template')

@section('content')

    <div class="certificado">
        <div class="center">
            <div class="informacoes">
                <h2>EMITA O SEU CERTIFICADO AQUI</h2>
                <p>Ao completar 75% de visualizações das aulas o sistema permitirá que você emita o certificado de acordo com os dados cadastrados aqui no hotsite.</p>
            </div>

            <div class="box">
            @if($user->aptoAoCertificado())
                <p>Parabéns! Você completou mais de 75% das aulas.</p>
                <a href="{{ route('certificado.emitir') }}">
                    EMITIR CERTIFICADO
                </a>
            @else
                <p>Você ainda não completou as aulas<br>para realizar a emissão do certificado.</p>
            @endif
            </div>
        </div>
    </div>

@endsection
