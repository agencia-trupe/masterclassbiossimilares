@extends('frontend.common.template')

@section('content')

    <div class="login">
        <div class="center">
            <h2>LOGIN</h2>
            <form action="{{ route('loginPost') }}" method="POST">
                @if(session('erro-login'))
                    <div class="erro">{{ session('erro-login') }}</div>
                @endif

                {!! csrf_field() !!}
                <input type="email" name="email" value="{{ old('email') }}" placeholder="e-mail" required>
                <input type="password" name="senha" placeholder="senha" required>
                <input type="submit" value="ENTRAR">
                <a href="{{ route('esqueci') }}" class="esqueci">esqueci minha senha &raquo;</a>
            </form>

            <div class="btn-cadastro">
                <h2>AINDA NÃO TENHO CADASTRO</h2>
                <a href="{{ route('cadastro') }}">PRIMEIRO CADASTRO</a>
            </div>
        </div>
    </div>

@endsection
