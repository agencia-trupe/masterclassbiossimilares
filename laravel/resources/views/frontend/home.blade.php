@extends('frontend.common.template')

@section('content')

    <div class="lista-aulas-grande">
        <div class="center">
            @foreach($aulas as $aula)
                <a href="{{ route('aulas', $aula->slug) }}">
                    <div class="imagem">
                        <img src="{{ asset('assets/img/videos/'.$aula->foto) }}" alt="">
                    </div>
                    <div class="texto">
                        <p>{{ $aula->autor }}</p>
                        <h3>{{ $aula->titulo }}</h3>
                        <p class="descricao">{!! strip_tags($aula->descricao, '<br>') !!}</p>
                    </div>
                </a>
            @endforeach
        </div>
    </div>

@endsection
