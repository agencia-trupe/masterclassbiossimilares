@extends('frontend.common.template')

@section('content')

    <div class="aulas">
        <div class="center">
            <div class="aula-exibicao">
                <div class="video-wrapper" data-video-id="{{ $aulaSelecionada->id }}">
                    <iframe src="https://player.vimeo.com/video/{{ $aulaSelecionada->video_codigo }}" width="640" height="360" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
                </div>
                <div class="descricao">
                    <div class="imagem">
                        <img src="{{ asset('assets/img/videos/'.$aulaSelecionada->foto) }}" alt="">
                    </div>
                    <div class="texto">
                        <h4>{{ $aulaSelecionada->autor }}</h4>
                        <h3>{{ $aulaSelecionada->titulo }}</h3>
                        <p>{!! strip_tags($aulaSelecionada->descricao, '<br>') !!}</p>
                    </div>
                </div>
                @if(count($aulaSelecionada->relacionados))
                <div class="outros-videos">
                    <h5>OUTROS VÍDEOS</h5>
                    <div>
                        @foreach($aulaSelecionada->relacionados as $relacionado)
                            <a href="{{ route('aulas', $relacionado->videoModel->slug) }}">
                                <img src="{{ asset('assets/img/videos/capa/'.$relacionado->videoModel->capa) }}" alt="">
                                <span class="icone-play"></span>
                            </a>
                        @endforeach
                    </div>
                </div>
                @endif
                @if(count($aulaSelecionada->downloads))
                <div class="downloads">
                    <h5>DOWNLOAD</h5>
                    @foreach($aulaSelecionada->downloads as $download)
                        <a href="{{ asset('downloads/'.$download->arquivo) }}" target="_blank">
                            {{ $download->titulo }}
                        </a>
                    @endforeach
                </div>
                @endif
            </div>
            <nav>
                @foreach($aulas as $aula)
                    <a href="{{ route('aulas', $aula->slug) }}" @if($aulaSelecionada->id == $aula->id) class="active" @endif>
                        <div class="imagem">
                            <img src="{{ asset('assets/img/videos/'.$aula->foto) }}" alt="">
                        </div>
                        <div class="texto">
                            <p>{{ $aula->autor }}</p>
                            <h3>{{ $aula->titulo }}</h3>
                        </div>
                    </a>
                @endforeach
            </nav>
        </div>
    </div>

@endsection
