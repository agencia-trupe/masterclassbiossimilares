@extends('frontend.common.template')

@section('content')

    <div class="cadastro">
        <div class="center">
            <form action="{{ route('cadastroPost') }}" method="POST">
                @if($errors->any())
                    <div class="erro">
                        @foreach($errors->all() as $error)
                        {{ $error }}<br>
                        @endforeach
                    </div>
                @endif

                {!! csrf_field() !!}
                <div class="row">
                    <label>nome</label>
                    <input type="text" name="nome" value="{{ old('nome') }}" required>
                </div>
                <div class="row">
                    <label>profissão</label>
                    <input type="text" name="profissao" value="{{ old('profissao') }}" required>
                </div>
                <div class="row">
                    <label>especialidade</label>
                    <input type="text" name="especialidade" value="{{ old('especialidade') }}" required>
                </div>
                <div class="row">
                    <label>registro profissional</label>
                    <input type="text" name="registro_profissional" value="{{ old('registro_profissional') }}" required>
                </div>
                <div class="row">
                    <label>cidade</label>
                    <input type="text" name="cidade" value="{{ old('cidade') }}" required>
                </div>
                <div class="row">
                    <label>telefone</label>
                    <input type="text" name="telefone" value="{{ old('telefone') }}" required>
                </div>
                <div class="row">
                    <label>e-mail</label>
                    <input type="email" name="email" value="{{ old('email') }}" required>
                </div>
                <div class="row">
                    <label>senha</label>
                    <input type="password" name="senha" required>
                </div>
                <div class="row">
                    <input type="submit" value="CADASTRAR">
                </div>
            </form>
        </div>
    </div>

@endsection
