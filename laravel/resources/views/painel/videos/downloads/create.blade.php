@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Vídeos / {{ $video->titulo }} / Downloads /</small> Adicionar Download</h2>
    </legend>

    {!! Form::open(['route' => ['painel.videos.downloads.store', $video->id], 'files' => true]) !!}

        @include('painel.videos.downloads.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
