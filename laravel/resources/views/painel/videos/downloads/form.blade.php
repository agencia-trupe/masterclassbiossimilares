@include('painel.common.flash')

<div class="form-group">
    {!! Form::label('titulo', 'Título') !!}
    {!! Form::text('titulo', null, ['class' => 'form-control']) !!}
</div>

<div class="well form-group">
    {!! Form::label('arquivo', 'Arquivo') !!}
    @if(isset($registro) && $registro->arquivo)
    <div style="margin-bottom:5px">
        <a href="{{ asset('downloads/'.$registro->arquivo) }}">{{ $registro->arquivo }}</a>
    </div>
    @endif
    {!! Form::file('arquivo', ['class' => 'form-control']) !!}
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}

<a href="{{ route('painel.videos.downloads.index', $video->id) }}" class="btn btn-default btn-voltar">Voltar</a>
