@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Vídeos / {{ $video->titulo }} / Downloads /</small> Editar Download</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.videos.downloads.update', $video->id, $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.videos.downloads.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
