@include('painel.common.flash')

<div class="form-group">
    {!! Form::label('relacionado_id', 'Vídeo Relacionado') !!}
    {!! Form::select('relacionado_id', $videosRelacionados, null, ['class' => 'form-control', 'placeholder' => 'Selecione']) !!}
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}

<a href="{{ route('painel.videos.relacionados.index', $video->id) }}" class="btn btn-default btn-voltar">Voltar</a>
