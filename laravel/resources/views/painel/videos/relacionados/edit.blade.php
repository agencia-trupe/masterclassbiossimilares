@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Vídeos / {{ $video->titulo }} / Relacionados /</small> Editar Relacionado</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.videos.relacionados.update', $video->id, $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.videos.relacionados.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
