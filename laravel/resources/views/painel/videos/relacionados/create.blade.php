@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Vídeos / {{ $video->titulo }} / Relacionados /</small> Adicionar Relacionado</h2>
    </legend>

    {!! Form::open(['route' => ['painel.videos.relacionados.store', $video->id], 'files' => true]) !!}

        @include('painel.videos.relacionados.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
