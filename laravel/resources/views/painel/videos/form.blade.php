@include('painel.common.flash')

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('titulo', 'Título') !!}
            {!! Form::text('titulo', null, ['class' => 'form-control']) !!}
        </div>

        <div class="form-group">
            {!! Form::label('autor', 'Autor') !!}
            {!! Form::text('autor', null, ['class' => 'form-control']) !!}
        </div>

        <div class="form-group">
            {!! Form::label('descricao', 'Descrição') !!}
            {!! Form::textarea('descricao', null, ['class' => 'form-control ckeditor', 'data-editor' => 'cleanBr']) !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="well form-group">
            {!! Form::label('foto', 'Foto') !!}
        @if($submitText == 'Alterar')
            <img src="{{ url('assets/img/videos/'.$registro->foto) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
        @endif
            {!! Form::file('foto', ['class' => 'form-control']) !!}
        </div>
        <div class="well">
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        {!! Form::label('video_tipo', 'Vídeo [Tipo]') !!}
                        {!! Form::select('video_tipo', ['vimeo' => 'Vimeo'], null, ['class' => 'form-control']) !!}
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        {!! Form::label('video_codigo', 'Vídeo [Código]') !!}
                        {!! Form::text('video_codigo', null, ['class' => 'form-control']) !!}
                    </div>
                </div>
            </div>
        </div>
        <div class="well form-group">
            {!! Form::label('capa', 'Capa') !!}
        @if($submitText == 'Alterar')
            <img src="{{ url('assets/img/videos/capa/'.$registro->capa) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
        @endif
            {!! Form::file('capa', ['class' => 'form-control']) !!}
        </div>
    </div>
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}

<a href="{{ route('painel.videos.index') }}" class="btn btn-default btn-voltar">Voltar</a>
