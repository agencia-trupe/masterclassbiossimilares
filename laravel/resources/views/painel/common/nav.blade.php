<ul class="nav navbar-nav">
    <li @if(str_is('painel.sobre*', Route::currentRouteName())) class="active" @endif>
        <a href="{{ route('painel.sobre.index') }}">Sobre o Programa</a>
    </li>
    <li @if(str_is('painel.videos*', Route::currentRouteName())) class="active" @endif>
        <a href="{{ route('painel.videos.index') }}">Vídeos</a>
    </li>
    <li @if(str_is('painel.palestrantes*', Route::currentRouteName())) class="active" @endif>
        <a href="{{ route('painel.palestrantes.index') }}">Palestrantes</a>
    </li>
    <li @if(str_is('painel.cadastros*', Route::currentRouteName())) class="active" @endif>
        <a href="{{ route('painel.cadastros.index') }}">Cadastros</a>
    </li>
    {{--<li class="dropdown @if(str_is('painel.contato*', Route::currentRouteName())) active @endif">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            Contato
            @if($contatosNaoLidos >= 1)
            <span class="label label-success" style="margin-left:3px;">{{ $contatosNaoLidos }}</span>
            @endif
            <b class="caret"></b>
        </a>
        <ul class="dropdown-menu">
            <li><a href="{{ route('painel.contato.index') }}">Informações de Contato</a></li>
            <li><a href="{{ route('painel.contato.recebidos.index') }}">
                Contatos Recebidos
                @if($contatosNaoLidos >= 1)
                <span class="label label-success" style="margin-left:3px;">{{ $contatosNaoLidos }}</span>
                @endif
            </a></li>
        </ul>
    </li>--}}
</ul>
