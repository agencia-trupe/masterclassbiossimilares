@extends('painel.common.template')

@section('content')

    @include('painel.common.flash')

    <legend>
        <h2>
            Cadastros
        </h2>
    </legend>


    @if(!count($registros))
    <div class="alert alert-warning" role="alert">Nenhum registro encontrado.</div>
    @else
    <table class="table table-striped table-bordered table-hover table-info">
        <thead>
            <tr>
                <th>Nome</th>
                <th>Aulas Concluídas</th>
                <th class="no-filter"><span class="glyphicon glyphicon-cog"></span></th>
            </tr>
        </thead>

        <tbody>
        @foreach ($registros as $registro)
            <tr class="tr-row" id="{{ $registro->id }}">
                <td>{{ $registro->nome }}</td>
                <td>
                    {{ $registro->fracaoAulasConcluidas }}
                    <span class="label label-{{ $registro->aptoAoCertificado() ?'success' : 'default' }}" style="margin-left:.5em;font-size:1em">{{ $registro->porcentagemAulasConcluidas }}%</span>
                </td>
                <td class="crud-actions">
                    <a href="{{ route('painel.cadastros.show', $registro->id ) }}" class="btn btn-primary btn-sm pull-left">
                        <span class="glyphicon glyphicon-align-left" style="margin-right:10px;"></span>Ver cadastro
                    </a>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>

    {!! $registros->render() !!}
    @endif

@endsection
