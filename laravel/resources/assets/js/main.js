import AjaxSetup from './AjaxSetup';
import MobileToggle from './MobileToggle';

AjaxSetup();
MobileToggle();

if ($('.aulas .video-wrapper').length) {
    var iframe = document.querySelector('iframe');
    var player = new Vimeo.Player(iframe);

    var videoId = $('.aulas .video-wrapper').data('video-id');

    player.on('ended', function() {
        $.ajax({
            url: $('base').attr('href') + '/aulas/' + videoId + '/concluida'
        });
    });
}
