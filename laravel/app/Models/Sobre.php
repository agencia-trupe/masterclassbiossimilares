<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class Sobre extends Model
{
    protected $table = 'sobre';

    protected $guarded = ['id'];

}
