<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class VideoRelacionado extends Model
{
    protected $table = 'videos_relacionados';

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }

    public function scopeVideo($query, $id)
    {
        return $query->where('video_id', $id);
    }

    public function videoModel()
    {
        return $this->belongsTo('App\Models\Video', 'relacionado_id');
    }
}
