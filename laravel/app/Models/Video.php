<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;

use Illuminate\Support\Facades\Cache;

class Video extends Model implements SluggableInterface
{
    use SluggableTrait;

    protected $sluggable = [
        'build_from' => 'titulo',
        'save_to'    => 'slug',
        'on_update'  => true
    ];

    protected $table = 'videos';

    protected $guarded = ['id'];

    public static function boot()
    {
        parent::boot();

        self::created(function($model) {
            Cache::forget('videos_count');
            return static::cachedCount();
        });

        self::deleted(function($model) {
            Cache::forget('videos_count');
            return static::cachedCount();
        });
    }

    public static function cachedCount()
    {
        return Cache::rememberForever('videos_count', function () {
            return static::count();
        });
    }

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }

    public static function upload_foto()
    {
        return CropImage::make('foto', [
            'width'  => 130,
            'height' => 130,
            'path'   => 'assets/img/videos/'
        ]);
    }

    public static function upload_capa()
    {
        return CropImage::make('capa', [
            'width'  => 230,
            'height' => 130,
            'path'   => 'assets/img/videos/capa/'
        ]);
    }

    public function downloads()
    {
        return $this->hasMany('App\Models\VideoDownload', 'video_id')->ordenados();
    }

    public function relacionados()
    {
        return $this->hasMany('App\Models\VideoRelacionado', 'video_id')->ordenados();
    }
}
