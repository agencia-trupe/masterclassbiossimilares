<?php

namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class Cadastro extends Model implements AuthenticatableContract, CanResetPasswordContract
{
    use Authenticatable, CanResetPassword;

    protected $table = 'cadastros';

    protected $guarded = ['id'];

    protected $hidden = ['senha', 'remember_token'];

    public function getAuthPassword()
    {
        return $this->senha;
    }

    public function videos()
    {
        return $this->belongsToMany(Video::class, 'cadastro_video', 'cadastro_id', 'video_id')->withTimestamps();
    }

    public function getAulasConcluidasAttribute()
    {
        return $this->videos->unique();
    }

    public function getFracaoAulasConcluidasAttribute()
    {
        return $this->aulasConcluidas->count().' / '. Video::cachedCount();
    }

    public function getPorcentagemAulasConcluidasAttribute()
    {
        return round(($this->aulasConcluidas->count() / Video::cachedCount()) * 100);
    }

    public function aptoAoCertificado()
    {
        return $this->porcentagemAulasConcluidas >= 75;
    }

    public function geraCertificado()
    {
        $dompdf = new \Dompdf\Dompdf();
        $dompdf->loadHtml(view('frontend.certificado-pdf', [
            'nome' => $this->nome
        ])->render());
        $dompdf->setPaper('A4', 'landscape');
        $dompdf->render();
        $dompdf->stream('MasterclassBiossimilares_Certificado.pdf');
    }
}
