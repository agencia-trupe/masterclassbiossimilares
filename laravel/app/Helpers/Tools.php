<?php

namespace App\Helpers;

class Tools
{

    public static function loadJquery()
    {
        return '<script src="//ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>'."\n\t".'<script>window.jQuery || document.write(\'<script src="' . asset("assets/vendor/jquery/dist/jquery.min.js") . '"><\/script>\')</script>';
    }

    public static function loadJs($path)
    {
        return '<script src="' . asset('assets/'.$path) .'"></script>';
    }

    public static function loadCss($path)
    {
        return '<link rel="stylesheet" href="' . asset('assets/'.$path) . '">';
    }

    public static function isActive($routeName)
    {
        return str_is($routeName, \Route::currentRouteName());
    }

    public static function formataData($data)
    {
        $meses = ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'];

        list($dia, $mes, $ano) = explode('/', $data);

        return $dia . ' de ' . $meses[(int) $mes - 1] . ' de ' . $ano;
    }

    public static function videoThumb($tipo, $id)
    {
        if (!file_exists(public_path('assets/img/videos/capa/'))) {
            mkdir(public_path('assets/img/videos/capa/'), 0777, true);
        }

        try {
            if ($tipo == 'vimeo') {

                $hash = unserialize(file_get_contents("http://vimeo.com/api/v2/video/$id.php"));
                $thumbURL = isset($hash[0]['thumbnail_large']) ? $hash[0]['thumbnail_large'] : false;

                $filename = $id.'_'.date('YmdHis').'.jpg';
                copy($thumbURL, 'assets/img/videos/capa/'.$filename);

                return $filename;

            } elseif ($tipo == 'youtube') {

                $thumbURL = "http://img.youtube.com/vi/$id/mqdefault.jpg";

                $filename = $id.'_'.date('YmdHis').'.jpg';
                copy($thumbURL, 'assets/img/videos/capa/'.$filename);

                return $filename;

            }
        } catch (\Exception $e) {
            throw new \Exception('Vídeo não encontrado.', 1);
        }

        return '';
    }

}
