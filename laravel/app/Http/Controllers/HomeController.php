<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Video;
use App\Models\Sobre;
use App\Models\Coordenadora;
use App\Models\Palestrante;

class HomeController extends Controller
{
    public function index()
    {
        $aulas = Video::ordenados()->get();

        return view('frontend.home', compact('aulas'));
    }

    public function login()
    {
        if (\Auth::guard('cadastro')->check()) {
            return redirect()->route('home');
        }

        return view('frontend.login');
    }

    public function cadastro()
    {
        if (\Auth::guard('cadastro')->check()) {
            return redirect()->route('home');
        }

        return view('frontend.cadastro');
    }

    public function esqueci()
    {
        return view('frontend.esqueci');
    }

    public function sobre()
    {
        $aulas = Video::ordenados()->get();
        $sobre = Sobre::first();

        return view('frontend.sobre', compact('aulas', 'sobre'));
    }

    public function aulas(Video $aula)
    {
        $aulas = Video::ordenados()->get();

        if (!count($aulas)) abort('404');

        $aulaSelecionada = $aula->exists ? $aula : $aulas->first();

        return view('frontend.aulas', compact('aulas', 'aulaSelecionada'));
    }

    public function aulaConcluida($id)
    {
        if (! request()->ajax()) {
            return response('Acesso não autorizado.', 401);
        }

        $aula = Video::findOrFail($id);
        $user = auth('cadastro')->user();

        if (! $user->videos->contains($id)) {
            $user->videos()->attach($id);
        }
    }

    function palestrantes()
    {
        $palestrantes = Palestrante::ordenados()->get();

        return view('frontend.palestrantes', compact('palestrantes'));
    }

    public function certificado()
    {
        $user = auth('cadastro')->user();

        return view('frontend.certificado', compact('user'));
    }

    public function certificadoEmitir()
    {
        $user = auth('cadastro')->user();

        if (! $user->aptoAoCertificado()) {
            return redirect()->route('certificado');
        }

        return $user->geraCertificado();
    }
}
