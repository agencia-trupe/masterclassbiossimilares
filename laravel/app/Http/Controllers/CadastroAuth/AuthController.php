<?php

namespace App\Http\Controllers\CadastroAuth;

use App\Models\Cadastro;
use Auth;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Illuminate\Http\Request;
use App\Http\Requests\CadastrosRequest;

class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/login';
    protected $guard      = 'cadastro';

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('guest', ['except' => 'logout']);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(CadastrosRequest $request)
    {
        $input = $request->all();
        $input['senha'] = bcrypt($request->get('senha'));

        $usuario = Cadastro::create($input);

        Auth::guard('cadastro')->loginUsingId($usuario->id);

        return redirect()->route('home');
    }

    protected function login(Request $request) {
        if (Auth::guard('cadastro')->attempt([
            'email'    => $request->get('email'),
            'password' => $request->get('senha')
        ])) {
            return redirect()->route('home');
        } else {
            return redirect()->route('loginCadastro')->withInput()->with('erro-login', 'e-mail ou senha inválidos');
        }
    }
}
