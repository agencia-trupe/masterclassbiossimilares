<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\VideosRequest;
use App\Http\Controllers\Controller;

use App\Models\Cadastro;

class CadastrosController extends Controller
{
    public function index()
    {
        $registros = Cadastro::with('videos')->paginate(20);

        return view('painel.cadastros.index', compact('registros'));
    }

    public function show(Cadastro $registro)
    {
        return view('painel.cadastros.show', compact('registro'));
    }
}
