<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\VideosDownloadsRequest;
use App\Http\Controllers\Controller;

use App\Models\Video;
use App\Models\VideoDownload;

class VideosDownloadsController extends Controller
{
    public function index(Video $video)
    {
        $registros = VideoDownload::video($video->id)->ordenados()->get();

        return view('painel.videos.downloads.index', compact('video', 'registros'));
    }

    public function create(Video $video)
    {
        return view('painel.videos.downloads.create', compact('video'));
    }

    public function store(Video $video, VideosDownloadsRequest $request)
    {
        try {

            $input = $request->all();

            if (isset($input['arquivo'])) $input['arquivo'] = VideoDownload::upload_arquivo();

            $video->downloads()->create($input);

            return redirect()->route('painel.videos.downloads.index', $video->id)->with('success', 'Registro adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: '.$e->getMessage()]);

        }
    }

    public function edit(Video $video, VideoDownload $registro)
    {
        return view('painel.videos.downloads.edit', compact('video', 'registro'));
    }

    public function update(Video $video, VideoDownload $registro, VideosDownloadsRequest $request)
    {
        try {

            $input = $request->all();

            if (isset($input['arquivo'])) $input['arquivo'] = VideoDownload::upload_arquivo();

            $registro->update($input);

            return redirect()->route('painel.videos.downloads.index', $video->id)->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);

        }
    }

    public function destroy(Video $video, VideoDownload $registro)
    {
        try {

            $registro->delete();

            return redirect()->route('painel.videos.downloads.index', $video->id)->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }

}
