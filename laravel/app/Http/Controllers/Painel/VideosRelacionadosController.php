<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\VideosRelacionadosRequest;
use App\Http\Controllers\Controller;

use App\Models\Video;
use App\Models\VideoRelacionado;

class VideosRelacionadosController extends Controller
{
    public function index(Video $video)
    {
        $registros = VideoRelacionado::video($video->id)->ordenados()->get();

        return view('painel.videos.relacionados.index', compact('video', 'registros'));
    }

    public function create(Video $video)
    {
        $videosRelacionados = Video::ordenados()
            ->where('id', '!=', $video->id)
            ->lists('titulo', 'id');

        return view('painel.videos.relacionados.create', compact('video', 'videosRelacionados'));
    }

    public function store(Video $video, VideosRelacionadosRequest $request)
    {
        try {

            $input = $request->all();

            $video->relacionados()->create($input);

            return redirect()->route('painel.videos.relacionados.index', $video->id)->with('success', 'Registro adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: '.$e->getMessage()]);

        }
    }

    public function edit(Video $video, VideoRelacionado $registro)
    {
        $videosRelacionados = Video::ordenados()
            ->where('id', '!=', $video->id)
            ->lists('titulo', 'id');

        return view('painel.videos.relacionados.edit', compact('video', 'registro', 'videosRelacionados'));
    }

    public function update(Video $video, VideoRelacionado $registro, VideosRelacionadosRequest $request)
    {
        try {

            $input = $request->all();

            $registro->update($input);

            return redirect()->route('painel.videos.relacionados.index', $video->id)->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);

        }
    }

    public function destroy(Video $video, VideoRelacionado $registro)
    {
        try {

            $registro->delete();

            return redirect()->route('painel.videos.relacionados.index', $video->id)->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }

}
