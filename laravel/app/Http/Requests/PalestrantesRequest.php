<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class PalestrantesRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'foto' => 'required|image',
            'nome' => 'required',
            'texto' => 'required',
        ];

        if ($this->method() != 'POST') {
            $rules['foto'] = 'image';
        }

        return $rules;
    }
}
