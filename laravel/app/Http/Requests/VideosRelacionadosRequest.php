<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class VideosRelacionadosRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'relacionado_id' => 'required',
        ];

        if ($this->method() != 'POST') {
        }

        return $rules;
    }
}
