<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class VideosRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'titulo' => 'required',
            'autor' => 'required',
            'descricao' => 'required',
            'foto' => 'required|image',
            'capa' => 'required|image',
            'video_tipo' => 'required',
            'video_codigo' => 'required',
        ];

        if ($this->method() != 'POST') {
            $rules['foto'] = 'image';
            $rules['capa'] = 'image';
        }

        return $rules;
    }
}
