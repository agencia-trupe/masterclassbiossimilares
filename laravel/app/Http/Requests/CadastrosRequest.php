<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class CadastrosRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nome'                  => 'required',
            'profissao'             => 'required',
            'especialidade'         => 'required',
            'registro_profissional' => 'required',
            'cidade'                => 'required',
            'telefone'              => 'required',
            'email'                 => 'required|email|unique:cadastros,email',
            'senha'                 => 'required|min:6',
        ];
    }

    public function messages() {
        return [
            'nome.required'                  => 'preencha seu nome',
            'profissao.required'             => 'preencha sua profissão',
            'especialidade.required'         => 'preencha sua especialidade',
            'registro_profissional.required' => 'preencha seu registro profissional',
            'cidade.required'                => 'preencha sua cidade',
            'telefone.required'              => 'preencha seu telefone',
            'email.required'                 => 'insira um endereço de e-mail válido',
            'email.email'                    => 'insira um endereço de e-mail válido',
            'email.unique'                   => 'o e-mail inserido já está cadastrado',
            'senha.required'                 => 'insira uma senha',
            'senha.confirmed'                => 'a confirmação de senha não confere',
            'senha.min'                      => 'sua senha deve ter no mínimo 6 caracteres',
        ];
    }
}
