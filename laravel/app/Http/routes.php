<?php

Route::group(['middleware' => ['web']], function () {
    Route::get('/', 'HomeController@index')->name('home');
    Route::get('sobre-o-programa', 'HomeController@sobre')->name('sobre');
    Route::get('palestrantes-e-coordenadora', 'HomeController@palestrantes')->name('palestrantes');

    // Páginas restritas
    Route::group(['middleware' => ['auth.cadastro']], function() {
        Route::get('aulas/{video_slug?}', 'HomeController@aulas')->name('aulas');
        Route::get('aulas/{video_id}/concluida', 'HomeController@aulaConcluida')->name('aulas.concluida');
        Route::get('certificado', 'HomeController@certificado')->name('certificado');
        Route::get('certificado/emitir', 'HomeController@certificadoEmitir')->name('certificado.emitir');
    });

    // Cadastro
    Route::get('login', 'HomeController@login')->name('loginCadastro');
    Route::post('login', 'CadastroAuth\AuthController@login')->name('loginPost');
    Route::get('cadastro', 'HomeController@cadastro')->name('cadastro');
    Route::post('cadastro', 'CadastroAuth\AuthController@create')->name('cadastroPost');
    Route::get('esqueci-minha-senha', 'HomeController@esqueci')->name('esqueci');
    Route::get('redefinicao-de-senha/{token}', 'CadastroAuth\PasswordController@showResetForm');
    Route::post('redefinicao-de-senha', 'CadastroAuth\PasswordController@sendResetLinkEmail')->name('redefinicao');
    Route::post('redefinir-senha', 'CadastroAuth\PasswordController@reset')->name('redefinir');
    Route::get('logout', 'CadastroAuth\AuthController@logout')->name('logoutCadastro');

    // Painel
    Route::group([
        'prefix'     => 'painel',
        'namespace'  => 'Painel',
        'middleware' => ['auth']
    ], function() {
        Route::get('/', 'PainelController@index')->name('painel');

        /* GENERATED ROUTES */
		Route::resource('palestrantes', 'PalestrantesController');
        Route::resource('videos', 'VideosController');
        Route::resource('videos.downloads', 'VideosDownloadsController');
        Route::resource('videos.relacionados', 'VideosRelacionadosController');
        Route::resource('sobre', 'SobreController', ['only' => ['index', 'update']]);
        Route::resource('configuracoes', 'ConfiguracoesController', ['only' => ['index', 'update']]);
        Route::resource('cadastros', 'CadastrosController');

        /*Route::get('contato/recebidos/{recebidos}/toggle', ['as' => 'painel.contato.recebidos.toggle', 'uses' => 'ContatosRecebidosController@toggle']);
        Route::resource('contato/recebidos', 'ContatosRecebidosController');
        Route::resource('contato', 'ContatoController');*/
        Route::resource('usuarios', 'UsuariosController');

        Route::post('ckeditor-upload', 'PainelController@imageUpload');
        Route::post('order', 'PainelController@order');
        Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');

        Route::get('generator', 'GeneratorController@index')->name('generator.index');
        Route::post('generator', 'GeneratorController@submit')->name('generator.submit');
    });

    // Auth
    Route::group([
        'prefix'    => 'painel',
        'namespace' => 'Auth'
    ], function() {
        Route::get('login', 'AuthController@showLoginForm')->name('auth');
        Route::post('login', 'AuthController@login')->name('login');
        Route::get('logout', 'AuthController@logout')->name('logout');
    });
});
